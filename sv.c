/*
	Projektarbeit Schiffe versenken
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include"split.c"
#include"farben.c"
//Größen Spielfeld
#define ZEILEN 20
#define SPALTEN 20
//Größen Menü
#define NUMBER 4
#define NUMBER_HEADER 5
#define LENGTH 50

//Inhalt von Menü
typedef struct menu
{
	char active[LENGTH];
	char idle[LENGTH];
} MENU;

//Inhalte Bestenliste
typedef struct Scoreboard 
{ 
	char username[50];
	int result; //Punktestand
} SCOREBOARD; 

//Prototypen Funktionen Hauptspiel
void erstelle_spielfeld(char [][SPALTEN]);
int random_erstellen(char [][SPALTEN]);
void spielen(char [][SPALTEN], char [][SPALTEN],int*, int *, SCOREBOARD [], char []);
void ausgabe_spielfeld(char [][SPALTEN]);
void ausgabe_spielfeld2(char [][SPALTEN], char [][SPALTEN]);

//Prototypen Funktionen Menü
void fill(MENU [NUMBER], char [NUMBER_HEADER][LENGTH], char [NUMBER][LENGTH]);
void menu_first_output(MENU [NUMBER], char [NUMBER_HEADER][LENGTH]);
void menu_down(MENU [NUMBER], char [NUMBER_HEADER][LENGTH], int *);
void menu_up(MENU [NUMBER], char [NUMBER_HEADER][LENGTH], int *);
void credits(char [NUMBER][LENGTH]);

//Prototypen Funktionen Bestenliste
void fill_struct_scoreboard(FILE *, SCOREBOARD [], int *);
void print_scoreboard(SCOREBOARD [], int);

int main(void) 
{
	//Variablen Hauptspiel
	char spielfeld_leer[ZEILEN][SPALTEN];
	char spielfeld_anzeige[ZEILEN][SPALTEN];
	int a; //Anzahl Schiffe
	int leer = 1;
	
	//Variablen Menü und Credits
	char menu_input; //Menüeingabe vom Nutzer
	int start = 0; //Eingabe bestätigt, für Spielstart
	int position = 1; //speichert auf welchem Punkt Menü aktuell steht
	MENU menu[NUMBER]; //Menü
	char menu_header[NUMBER_HEADER][LENGTH]; //Menükopf
	char credits[NUMBER][LENGTH]; //Impressum	

	//Weitere Variablen
	int i = 0;
	char name[LENGTH]; //Nutzername
	char name_tmp[LENGTH]; //Nutername temopraer

	//Variablen Bestenliste
	SCOREBOARD board[16];
	int counter = 0; //Anzahl Datensätze
	FILE *fp_sb;

	//Bestenliste laden
	fp_sb = fopen(".score.csv","r");

	if(fp_sb == NULL) 
    	{ 
        	printf("Notwendige Datei score.csv nicht gefunden!\n"); 
		exit(100);
    	} 
	fill_struct_scoreboard(fp_sb, board, &counter);
	fclose(fp_sb);

	//Spielername festlegen
	printf("Bitte gebe Deinen Namen ein:");
	do
	{
		gets(name_tmp);
		if(strlen(name_tmp) > 50)
		{
			printf("Leider ist Dein Name zu lang - Versuche es erneut!");
		}
		else
		{
			strcpy(name, name_tmp);
		}
	} while(strlen(name_tmp)>50);

	//Aufrufe Menue und Credits
	fill(menu, menu_header, credits); //Fuellen von Menue und Credits
	menu_first_output(menu, menu_header);
	

	do 
	{
		system("stty raw");
		menu_input = getchar();
		system("stty cooked");
		if(menu_input == 'w')
		{
			menu_up(menu, menu_header, &position);
		} 
		else if (menu_input == 's')
		{
			menu_down(menu, menu_header, &position);
		} 
		else if (menu_input == 'f') 
		{
			if(position == 1)
			{
				system("clear");
				//Aufrufe Hauptspiel
				erstelle_spielfeld(spielfeld_leer);
				a = random_erstellen(spielfeld_anzeige);
				spielen(spielfeld_leer, spielfeld_anzeige,&a,&counter,board, name);
				ausgabe_spielfeld2(spielfeld_anzeige,spielfeld_leer);
				start == 1;
			}
			else if(position == 2)
			{
				print_scoreboard(board, counter);		
			}
			else if(position == 3)
			{
				system("clear");
				for(i=0;i<NUMBER;i++)
				{
					printf(BLUE "%s\n" RESET, credits[i]);
				}
			}
			else if(position == 4)
			{
				system("clear");
				exit(0);
			}
		} 
		else 
		{
			system("clear");
			printf("Eingabe nicht erkannt. w: Menü Hoch; s: Menü Runter; f: Neues Spiel\n");
		}
	}while(start != 1);

	system("clear");

}

int random_erstellen(char s_anzeige[][SPALTEN])
{	
	//Variablen
	int a = 0; //Anzahl Schiffe
	int i = 0; //zähler
	int p = 0; //position des schiffes
	int z = 0; //zähler bis Schiffslänge erreicht ist
	int zahl = 0; // 1-3 schiff liegt waagerecht, 4-6 schiff liegt senkrecht
	int tmp = 0; //temp zur Abfrage ob schifflänge ok
	int size = 0; //zuordnung der Schiffsgröße
	int zeile = 0;
	int spalte = 0;
	
	for(zeile = 0;zeile < ZEILEN; zeile++)
	{
		for (spalte = 0;spalte < SPALTEN; spalte++)
		{
			s_anzeige[zeile][spalte] = '.';
  		}
	}		
	srand(time(0));
	a = (rand() % ((12 + 1) - 7)) + 7;
	for(i=0; i < a; i++)
	{
		zahl= rand() % 6 + 1;
		if(zahl < 4) //Schiff wird waagerecht gelegt
		{
			size = rand() % 3 +1; //Schiffgröße 2, 3, 5 bestimmen
			if(size == 1)
			{
				
				p = rand() % 20;
				tmp = rand() % 19;
					while(s_anzeige[p][tmp] == 'x' || s_anzeige[p][tmp+1] == 'x')
					{
						tmp = rand() % 19;
						p = rand() % 20;
						//usleep(10000);
					}
				for(z = 0; z < 2; z++)
				{	
					s_anzeige[p][tmp] = 'x';
					tmp++;
				}
			}	
			else if(size == 2)
			{
				p = rand() % 20;
				tmp = rand() % 18;
					while(s_anzeige[p][tmp] == 'x' || s_anzeige[p][tmp+1] == 'x' || s_anzeige[p][tmp+2] == 'x')
					{
						tmp = rand() % 18;
						p = rand() % 20;
					}
				for(z = 0; z < 3; z++)
				{
				s_anzeige[p][tmp] = 'x';
				tmp++;
		}
			}
			else 
			{
				p = rand() % 20;
				tmp = rand() % 16;
					while(s_anzeige[p][tmp] == 'x' || s_anzeige[p][tmp+1] == 'x' || s_anzeige[p][tmp+2] == 'x' || s_anzeige[p][tmp+3] == 'x' || s_anzeige[p][tmp+4] == 'x')
					{
						tmp = rand() % 16;
						p = rand() % 20;
					}
				for(z = 0; z < 5; z++)
				{
					s_anzeige[p][tmp] = 'x';
					tmp++;
				}
			}
		}
		else  //Schiff wird senkrecht gelegt
		{
			size = rand() % 3 +1; //schiffgröße 2, 3, 5 bestimmen
			if(size == 1)
			{
				p = rand() % 20;
				tmp = rand() % 19;
					while(s_anzeige[tmp][p] == 'x' || s_anzeige[tmp+1][p] == 'x')
					{						
						tmp = rand() % 19;
						p = rand() % 20;
					}
				for(z = 0; z < 2; z++)
				{
					s_anzeige[tmp][p] = 'x';
					tmp++;
				}
			}		
			else if(size == 2)
			{
				p = rand() % 20;
				tmp = rand() % 18;
					while(s_anzeige[tmp][p] == 'x' || s_anzeige[tmp+1][p] == 'x' || s_anzeige[tmp+2][p] == 'x')
					{
						tmp = rand() % 18;
						p = rand() % 20;
					}
				for(z = 0; z < 3; z++)
				{	
					s_anzeige[tmp][p] = 'x';
					tmp++;
				}
			}
			else 
			{
				p = rand() % 20;
				tmp = rand() % 16;
					while(s_anzeige[tmp][p] == 'x' ||  s_anzeige[tmp+1][p] == 'x' || s_anzeige[tmp+2][p] == 'x' || s_anzeige[tmp+3][p] == 'x' || s_anzeige[tmp+4][p] == 'x')
					{
						tmp = rand() % 16;
						p = rand() % 20;
					}
				for(z = 0; z < 5; z++)
				{
				
					s_anzeige[tmp][p] = 'x';
					tmp++;
				}
			}
		}		
	}
	return a;	
}
void spielen(char s_leer[][SPALTEN], char s_anzeige[][SPALTEN],int *a, int *co, SCOREBOARD sb[], char name[])
{

	int y = 0; //runden hochzählen
	int x = 0;//anzahl runden
 	int xko = 0;//x koordinate 
 	int yko = 0;//y koordinate
	char input;//Tasteneingabe zur koordinierung im mehrd. Array
	int positionx = 0;//aktuelle position x im mehrd. Array
	int positiony = 0;//aktuelle position y im mehrd. Array
	int v = 0;//übergabe von feuerbefehl
	int q = 0;//spiel beenden
	int z = 0;//zähler Zeilen
	int s = 0;//Zähler Spalten
	int c = 0;//zähler anzahl x im Anzeigefeld
	int p = 0;//vergleich mit Anzahl x
	int punkte = 0;//punkte im spiel erworben
	FILE *fp_sb;
	int i = 0;

	switch(*a) //anzahl Schiffe
	{
		case 7:
			x = 150;
			break;
		case 8: 
			x = 158;
			break;
		case 9:
			x = 166;
			break;
		case 10:
			x = 174;
			break;
		case 11:
		        x = 182;
			break;
		case 12:
			x = 190;
			break;
		default:
			break;
	}
	printf("Nutze wasd um dich über das Spielfeld zu bewegen und f um ein Feld abzuschießen\n(x zum Beenden drücken)\n");
	for (z = 0; z < ZEILEN; z++)
	{
		for(s = 0;s < SPALTEN; s++)//Anzahl x im Spielfeld Zählen
		{
			if(s_anzeige[z][s] == 'x')
			{
				c++;
			}
		}
	}
	ausgabe_spielfeld(s_leer);
	while(y < x && p < c && q == 0)
	{
		do
		{	
			system("stty raw");
			input = getchar();
			system("stty cooked");
			if(input == 'x')
			{
				q = 1;
				v = 2;
				y= 1;
			}
			else if(input == 'w')
			{
  		    		system("clear");
				printf("%i. Runde von %i Runden\n",y+1,x);
				if(positiony <= 0)
				{
					while(positiony < 19)
					{
						positiony++;
					}
					positionx--;
				}
				else
				{
					positiony -= 1;
				}
				if(s_leer[yko+positiony][xko+positionx] == 'o')
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = 'o';
				}
				else if(s_leer[yko+positiony][xko+positionx] == 'x')
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = 'x';
				}
				else
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = '.';
				}
			}
			else if(input == 'a')
			{	
				system("clear");
				printf("%i. Runde von %i Runden\n",y+1,x);
				if(positionx <= 0 && positiony <= 0)
				{
				
					while(positionx < 19 && positiony < 19)
					{
						positionx++;
						positiony++;
					}
				}
				else
				{
  		    			positionx -= 1;
				}
				if(s_leer[yko+positiony][xko+positionx] == 'o')
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = 'o';
				}
				else if(s_leer[yko+positiony][xko+positionx] == 'x')
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = 'x';
				}
				else
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = '.';
				}
			}
			else if(input == 's')
			{	
				system("clear");
				printf("%i. Runde von %i Runden\n",y+1,x);
				if(positiony >= 19)
				{
					while(positiony > 0)
					{
						positiony--;
					}
					positionx++;
				}
				else
				{
  		    			positiony += 1;
				}
				if(s_leer[yko+positiony][xko+positionx] == 'o')
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = 'o';
				}
				else if(s_leer[yko+positiony][xko+positionx] == 'x')
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = 'x';
				}
				else
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = '.';
				}
				
  		    	}
			else if(input == 'd')
			{	
				system("clear");
				printf("%i. Runde von %i Runden\n",y+1,x);
				if(positionx >= 19 && positiony >= 19)
				{
					while(positionx > 0 && positiony > 0)
					{
						positionx--;
						positiony--;
					}
				}
				else
				{
  		    			positionx += 1;
				}
				if(s_leer[yko+positiony][xko+positionx] == 'o')
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = 'o';
				}
				else if(s_leer[yko+positiony][xko+positionx] == 'x')
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = 'x';
				}
				else
				{
  		    			s_leer[yko+positiony][xko+positionx] = '_';
					ausgabe_spielfeld(s_leer);
  		    			s_leer[yko+positiony][xko+positionx] = '.';
				}
  		    	}
			else if(input == 'f')
			{
  	 			if(s_leer[yko+positiony][xko+positionx] == 'x'|| s_leer[yko+positiony][xko+positionx] == 'o')
				{
					printf("\nFeld bereits beschossen\n");
				}
				else
				{
					v = 1;	
				}
			}
		}while(v == 0);
		if(v == 1)
		{
  	 		system("clear");
  			if(s_anzeige[yko+positiony][xko+positionx] == 'x') 
  			{ 
  				s_leer[yko+positiony][xko+positionx] = 'x';
				ausgabe_spielfeld(s_leer);
  				printf("Getroffen! \n");
				p++;
  			}
  			else 
			{
				s_leer[yko+positiony][xko+positionx] = 'o';
				ausgabe_spielfeld(s_leer);
				printf("nicht Getroffen! \n");
			}
		}
		v = 0;
       	y++;
 	}
	printf("%i von %i Schiffsteilen getroffen\n",p,c);
	if(p == c)
	{
		//In Bestenliste schreiben
		printf("Du hast Gewonnen!\n");
		punkte = (x - y)*10;
		printf("%i",punkte);
		fp_sb = fopen(".score.csv", "w");
		for(i=0;i<=*co;i++)
		{
			fprintf(fp_sb, "%s;%i", sb[i].username, sb[i].result);
		}	
		fprintf(fp_sb, "%s;%i", name, punkte);
		*co += 1;
	}
	else
	{
		printf("Du hast leider verloren :( \n");
	}
}
void erstelle_spielfeld(char s_leer[][SPALTEN])
{
	int z = 0;
	int s = 0;
	
	for(z = 0;z < ZEILEN; z++)
	{
		for (s = 0;s < SPALTEN; s++)
		{
			s_leer[z][s] = '.';
  		}
	}		
}


void ausgabe_spielfeld(char s_leer[][SPALTEN])
{	
	int z = 0;
	int s = 0;
	for (z = 0; z < ZEILEN; z++)
	{
		for(s = 0;s < SPALTEN; s++)
		{
			printf("%2c",s_leer[z][s]);
  		}
		printf("\n");
	}
	printf("\n");
}
void ausgabe_spielfeld2(char s_anzeige[][SPALTEN],char s_leer[][SPALTEN])
{	
	int z = 0;
	int s = 0;
	for (z = 0; z < ZEILEN; z++)
	{
		for(s = 0;s < SPALTEN; s++)
		{	
  			if(s_leer[z][s] == 'x')
			{
				printf(RED "%2c" RESET,s_anzeige[z][s]);//Ausgabe abgeschossener Schiffsteile
			}
			else if(s_leer[z][s] == 'o')
			{
				s_anzeige[z][s] = 'o';
				printf(MAGENTA "%2c" RESET,s_anzeige[z][s]);//Ausgabe verfehlter Schüsse
			}
			else
			{
				printf("%2c",s_anzeige[z][s]);
			}
  		}
		printf("\n");
	}
}

void fill(MENU m[NUMBER], char h[NUMBER_HEADER][LENGTH], char c[NUMBER][LENGTH])
{
	strcpy(m[0].active, RRED WHITE BRIGHT "-> Spiel starten <-" RESET RESET RESET);
	strcpy(m[0].idle, BBLACK WHITE "   Spiel starten   " RESET RESET);
	strcpy(m[1].active, RRED WHITE BRIGHT "-> Bestenliste <-  " RESET RESET RESET);
	strcpy(m[1].idle, BBLACK WHITE "   Bestenliste     " RESET RESET);	
	strcpy(m[2].active, RRED WHITE BRIGHT "-> Credits <-      " RESET RESET RESET);
	strcpy(m[2].idle, BBLACK WHITE "   Credits         " RESET RESET);
	strcpy(m[3].active, RRED WHITE BRIGHT"-> Beenden <-      " RESET RESET RESET);
	strcpy(m[3].idle, BBLACK WHITE "   Beenden         " RESET RESET);
	strcpy(h[0], "--- Schiffe versenken ---");
	strcpy(h[1], "---     Hauptmenü     ---");
	strcpy(h[2], "w: Menü nach oben");
	strcpy(h[3], "s: Menü nach unten");
	strcpy(h[4], "f: Menüpunkt wählen");	
	strcpy(c[0], "Schiffe versenken - Schulprojekt im Fach C");
	strcpy(c[1], "Version: 1.0");
	strcpy(c[2], "Ben Gevaerts");
	strcpy(c[3], "Jens Heyn");
}

void menu_first_output(MENU m[NUMBER], char h[NUMBER_HEADER][LENGTH])
{
	int i = 0;

	system("clear");

	for(i=0;i<NUMBER_HEADER;i++)
	{
		printf(BLUE "%s" RESET, h[i]);
		printf("\n");
	}

	printf("\n");

	printf("%s\n%s\n%s\n%s\n", m[0].active, m[1].idle, m[2].idle, m[3].idle);
}

void menu_down(MENU m[NUMBER], char h[NUMBER_HEADER][LENGTH], int *pos)
{
	int i = 0;

	system("clear");

	//Ausgabe Menükopf
	for(i=0;i<NUMBER_HEADER;i++)
	{
		printf(BLUE "%s" RESET, h[i]);
		printf("\n");
	}

	printf("\n");
	
	//Änderung der aktuellen Menüposition mit entsprechender Ausgabe
	switch(*pos) {
		case 1: printf("%s\n%s\n%s\n%s\n", m[0].idle, m[1].active, m[2].idle, m[3].idle); 
			*pos = 2;
			break;
		case 2: printf("%s\n%s\n%s\n%s\n", m[0].idle, m[1].idle, m[2].active, m[3].idle); 
			*pos = 3;
			break;
		case 3: printf("%s\n%s\n%s\n%s\n", m[0].idle, m[1].idle, m[2].idle, m[3].active); 
			*pos = 4;
			break;
		case 4: printf("%s\n%s\n%s\n%s\n", m[0].active, m[1].idle, m[2].idle, m[3].idle);
			*pos = 1;
		default:
			break;
	}	
}

void menu_up(MENU m[NUMBER], char h[NUMBER_HEADER][LENGTH], int *pos)
{
	int i = 0;

	system("clear");

	//Ausgabe Menükopf
	for(i=0;i<NUMBER_HEADER;i++)
	{
		printf(BLUE "%s" RESET, h[i]);
		printf("\n");
	}

	printf("\n");
	
	//Änderung der aktuellen Menüposition mit entsprechender Ausgabe
	switch(*pos) 
	{
		case 1: printf("%s\n%s\n%s\n%s\n", m[0].idle, m[1].idle, m[2].idle, m[3].active); 
			*pos = 4;
			break;
		case 2: printf("%s\n%s\n%s\n%s\n", m[0].active, m[1].idle, m[2].idle, m[3].idle); 
			*pos = 1;
			break;
		case 3: printf("%s\n%s\n%s\n%s\n", m[0].idle, m[1].active, m[2].idle, m[3].idle); 
			*pos = 2;
			break;
		case 4: printf("%s\n%s\n%s\n%s\n", m[0].idle, m[1].idle, m[2].active, m[3].idle); 
			*pos = 3;
			break;
		default: 
			break;
	}

}

void fill_struct_scoreboard(FILE *fp_sb, SCOREBOARD sb[], int *c) 
{ 
    int offset = 0; 
	int i = 0;
    char tmp[30]; 
    char line[100] = {0}; 

    while(fgets(line,99,fp_sb) != NULL) 
    { 
        offset = split(sb[i].username,line,';')+1; 
	offset += split(tmp,line + offset, '\n');
	sb[i].result = atoi(tmp);
	(*c)++;
	i++;
    } 
} 

void print_scoreboard(SCOREBOARD sb[], int c) //Ausgabe der Bestenliste
{
	int i = 0;

	system("clear");
	
	printf(BLUE "Bestenliste nach Datum\nNutzername | Punkte\n" RESET);

	for(i=0; i<c; i++)
	{
		printf("%s | %i\n", sb[i].username, sb[i].result);
	}
}
